#ifndef MULTIAUDIO_PLAYER_HPP
#define MULTIAUDIO_PLAYER_HPP

#include <sndfile.hh>
#include <thread>
#include <string>
#include <stop_token>
#include <stdexcept>
#include <utility>
#include <portaudiocpp/Device.hxx>
#include <portaudiocpp/System.hxx>
#include <Eigen/Core>
#include <atomic>

namespace multiaudio {

class player
{
    SndfileHandle sndfile_handle;
    Eigen::MatrixXf channel_mapping;
    std::atomic_bool repeat;
    portaudio::Device *playback_device;
    unsigned long frames_per_buffer;

    std::jthread playing_thread;

    class play_wrapper {
        player *self;

    public:
        play_wrapper(player *self) : self(self) {}

        void operator ()(std::stop_token &&stop) {
            self->play(std::move(stop));
        }
    };

public:
    player()
        : sndfile_handle(),
          channel_mapping(),
          repeat(),
          playback_device(),
          playing_thread()
    {}

    template <typename MatrixType>
    player (
        const std::string &sound_file,
        MatrixType &&channel_mapping,
        bool repeat = false,
        portaudio::Device &playback_device = portaudio::System::instance().defaultOutputDevice(),
        unsigned long frames_per_buffer = 512
    )
        : sndfile_handle(sound_file),
          channel_mapping(std::forward<MatrixType>(channel_mapping)),
          repeat(repeat),
          playback_device(&playback_device),
          frames_per_buffer(frames_per_buffer),
          playing_thread()
    {
        if (sndfile_handle.error()) {
            throw std::runtime_error("Failed to open sound file");
        }
        if (sndfile_handle.channels() < this->channel_mapping.cols()) {
            throw std::runtime_error("The sound file does not have enough channels");
        }
    }

    void play(std::stop_token &&stop_token = std::stop_token());

    player &start() {
        playing_thread = std::jthread(play_wrapper(this));
        return *this;
    }

    player &stop() {
        playing_thread.request_stop();
        return *this;
    }

    player &stop_repeating() {
        repeat = false;
        return *this;
    }

    player &join() {
        playing_thread.join();
        return *this;
    }
};

}

#endif // MULTIAUDIO_PLAYER_HPP
