#include "multiaudio_player.hpp"

#include <portaudiocpp/SampleDataFormat.hxx>
#include <portaudiocpp/DirectionSpecificStreamParameters.hxx>
#include <portaudiocpp/StreamParameters.hxx>
#include <portaudiocpp/BlockingStream.hxx>
#include <portaudiocpp/Exception.hxx>
#include <cassert>

namespace multiaudio {

void player::play(std::stop_token &&stop_token) {
    using portaudio::SampleDataFormat;
    using portaudio::DirectionSpecificStreamParameters;
    using portaudio::StreamParameters;
    using portaudio::BlockingStream;

    assert(playback_device != nullptr);
    int sndfile_channels = channel_mapping.cols();
    int output_channels = channel_mapping.rows();
    DirectionSpecificStreamParameters outputParams(
                *playback_device,
                output_channels,
                SampleDataFormat::FLOAT32,
                true,
                playback_device->defaultHighOutputLatency(),
                nullptr);
    StreamParameters streamParams(
                DirectionSpecificStreamParameters::null(),
                outputParams,
                sndfile_handle.samplerate(),
                frames_per_buffer,
                0);
    BlockingStream stream(streamParams);

    int actual_sndfile_channels = sndfile_handle.channels();
    std::vector<float> sndfile_buffer(frames_per_buffer * actual_sndfile_channels);
    std::vector<float> output_buffer(frames_per_buffer * output_channels);
    sf_count_t frames_read;

    stream.start();

    bool stop_requested = false;
    do {
        sndfile_handle.seek(0, SEEK_SET);
        while( (stop_requested = stop_token.stop_requested()) == false ) {
            frames_read = sndfile_handle.readf(sndfile_buffer.data(), frames_per_buffer);
            if (frames_read <= 0) {
                break;
            }
            for (sf_count_t frame = 0; frame < frames_read; frame++) {
                for (int outchan = 0; outchan < output_channels; outchan++) {
                    output_buffer[frame * output_channels + outchan] = 0;
                    for (int sndchan = 0; sndchan < sndfile_channels; sndchan++) {
                        output_buffer[frame * output_channels + outchan]
                                += sndfile_buffer[frame * actual_sndfile_channels + sndchan]
                                   * channel_mapping(outchan, sndchan);
                    }
                }
            }
            try {
                stream.write(output_buffer.data(), frames_read);
            }
            catch (const portaudio::PaException &paException) {
                if (paException.paError() == paOutputUnderflowed) {
                    // ignore
                }
                else {
                    throw paException;
                }
            }
        }
    }
    while (repeat && !stop_requested);

    if (stop_token.stop_requested()) {
        stream.abort();
    }
    else {
        stream.stop();
    }
}

}
